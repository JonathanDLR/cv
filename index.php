<?php
header('Content-Type: text/html; charset=utf-8');
require_once($_SERVER['DOCUMENT_ROOT'].'/controller/AppController.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/controller/ContactController.php');

$appController = new AppController();
$contactController = new ContactController();

if(isset($_POST)) {
    if(isset($_POST['nom']) && isset($_POST['mail']) && isset($_POST['object']) && isset($_POST['content'])) {
        $contactController->getContact();
    }
}

$appController->getView();

?>

