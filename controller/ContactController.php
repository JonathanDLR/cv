<?php
header('Content-Type: text/html; charset=utf-8');
require_once($_SERVER['DOCUMENT_ROOT'].'/cv/backend/Contact.php');

class ContactController
{
    public function correction($texte)
    {
        $texte = htmlspecialchars($texte);
        $texte = stripslashes($texte);

        return $texte;
    }

    public function getContact()
    {
        if(isset($_POST)) {
            if(isset($_POST['nom']) && isset($_POST['mail']) && isset($_POST['object']) && isset($_POST['content'])) {
                $nom = $this->correction($_POST['nom']);
                $mail = filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL);
                $object = $this->correction($_POST['object']);
                $content = $this->correction($_POST['content']);
        
                $contact = new Contact();
                $contact->sendContact($nom, $mail, $object, $content);  
            }
        }
    }
}
?>