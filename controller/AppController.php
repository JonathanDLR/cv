<?php

class AppController
{
    public function getView()
    {
        include('frontend/common/head.html');
        include('frontend/view/body.html');
        include('frontend/common/script.html');
    }
}