$('a[href^="#"]').click(function(){ // SCROLL
	var the_id = $(this).attr("href");
	if (the_id === '#') {
		return;
	}
	$('html, body').animate({
		scrollTop:$(the_id).offset().top
	}, 'slow');
	return false;
});

// FLECHE NAVIGATION MOBILE

function flecheResize() {
	if (window.matchMedia("(max-width: 600px)").matches) {
		$('#flecheHaut img').attr('src', 'img/flecheMobile.png');
		$('#flecheBas img').attr('src', 'img/flecheBasMobile.png');
	}
	else {
		$('#flecheHaut img').attr('src', 'img/fleche.png');
		$('#flecheBas img').attr('src', 'img/flecheBas.png');
	}
}

window.addEventListener('resize', flecheResize);

flecheResize();

// MENU MOBILE

$('#menu').hide();

$('#menuBar').on('click', function() {
	$('#menu').toggle("slide", {direction: "left"}, 1000);
});

// SLIDE DES SECTIONS AU SCROLL

$(document).ready(function() {

	$('#competence').show("slide", {direction: "right"}, 2000);

	$(window).scroll(function() {
  	if ($(window).scrollTop() > $(document).height()*0.4) {
    	$('#experience').show('slide', {direction: "left"}, 2000);
  	}

		if ($(window).scrollTop() > $(document).height()*0.7) {
			$('#formation').show('slide', {direction: "right"}, 2000);
		}

		if ($(window).scrollTop() > $(document).height()*0.8) {
			$('#loisir').show('slide', {direction: "left"}, 2000);
		}
	});

});


// APPARITION SECTION AUX CLICK MENU


function apparition(section) { // APPARITION DE LA SECTION VOULUE
	section.css('display', 'flex');
}


function navigate(anchor, direction) { // LANCEMENT DU SLIDE ET NAVIGATION VERS LA SECTION VOULUE
  anchor.show('slide', {direction: direction}, 3000);
  $('html, body').animate({scrollTop: anchor.offset().top});
}

function navigateMobile(anchor, direction) { // LANCEMENT DU SLIDE ET NAVIGATION VERS LA SECTION VOULUE (MOBILE)
  anchor.show('slide', {direction: direction}, 3000);
  $('html, body').animate({scrollTop: anchor.offset().top - $('#menu').height()});
	$('#menu').hide();
}

$('.menuComp').on('click', function() { // SECTION COMPETENCE DIFFERENTE CAR APPARAIT AU CHARGEMENT
	if (window.matchMedia("(max-width: 600px)").matches) {
		$('html, body').animate({scrollTop: $('#competence').offset().top - $('#menu').height()
		}, 'slow');
		$('#menu').hide();
	}
});

// MISE EN PLACE DES EVENT

$('.menuExp').on('click', function() {
	if (window.matchMedia("(max-width: 600px)").matches) {
		navigateMobile($('#experience'), 'left');
	}
	else {
	navigate($('#experience'), 'left');
	}
});

$('.menuForm').on('click', function() {
	apparition($('#experience'));
	if (window.matchMedia("(max-width: 600px)").matches) {
		navigateMobile($('#formation'), 'right');
	}
	else {
		navigate($('#formation'), 'right');
	}
});

$('.menuLoisir').on('click', function() {
	apparition($('#experience'));
	apparition($('#formation'));
	if (window.matchMedia("(max-width: 600px)").matches) {
		navigateMobile($('#loisir'), 'left');
	}
	else {
		navigate($('#loisir'), 'left');
	}
});

// APPARITION DE TOUTES LES SECTIONS LORS DU CLICK SUR LA FLECHE BAS

$('#flecheBas').on('click', function() {
	apparition($('#experience'));
	apparition($('#formation'));
	apparition($('#loisir'));
	$('html, body').animate({scrollTop: $('#footer').offset().top});
});
