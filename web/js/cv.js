INIT = { // OBJET DE CHARGEMENT
	init: function() {
		SCROLL.init();
		NAVFIXED.init();
		MENUMOB.init();
		SHOW.init();
	}
},

SCROLL = { // Objet permettant le scroll
	init: function() {
		$('a[href^="#"]').on('click', SCROLL.scroll); // ON SELECTIONNE TOUS LES LIENS QUI ONT UN HREF
		$(window).on('load', SCROLL.slider);
	},

	scroll: function() { // SCROLL
		var the_id = $(this).attr("href");
		if (the_id === '#') {
			return;
		}
		$('html, body').animate({
			scrollTop:$(the_id).offset().top
		}, 'slow');
		return false;
	},

	slider: function() { // APPARITION DES DIV SELON LA POS DU SCROLL
		$('#competence').show("slide", {direction: "right"}, 2000);

		$(window).scroll(function() {
			if ($(window).scrollTop() > $(document).height()*0.1) {
				$('#experience').show('slide', {direction: "left"}, 2000);
			}

			if ($(window).scrollTop() > $(document).height()*0.5) {
				$('#formation').show('slide', {direction: "right"}, 2000);
			}

			if ($(window).scrollTop() > $(document).height()*0.6) {
				$('#loisir').show('slide', {direction: "left"}, 2000);
			}
		});
	}
},

NAVFIXED = {

	positionNav: $("nav").offset().top,

	init: function() {
		$(window).on("scroll", NAVFIXED.scrolledNav);
	},

	scrolledNav: function() {
		if ($(document).scrollTop() > NAVFIXED.positionNav) {
			$("nav").css("position", "fixed");
			$('nav').css("top", 0);
		}
		else {
			$("nav").css("position", "static");
		}
	}
},

MENUMOB = { // MENU MOBILE
	init: function() {
		$('#menu').hide(); // ON CACHE LE MENU PAR DEFAUT
		$(window).resize(function() { // ON CACHE LE MENU AU REDIMENSIONNEMENT
			$('#menu').hide();
			$('#container').show(); // ON REFAIT APPARAITRE LE CONTENT
		});
		$('#menuBar').on('click', function() {
			MENUMOB.toggler();
			MENUMOB.hideContainer();
		});
	},

	 toggler: function() { // TOGGLE DU MENU
		$('#menu').toggle("slide", {direction: "left"}, 1000);
	},

	hideContainer: function() { // TOGLE DU CONTAINER
		$('#container').toggle();
	}
},

SHOW = { // APPARITION SECTION AUX CLICK MENU

	init: function() {
		SHOW.apparition($('#competence'));
		$('.menuComp').on('click', function() {
			SHOW.navigate($('#competence'), 'right');
		});
		$('.menuExp').on('click', function() {
			SHOW.navigate($('#experience'), 'left');
		});
		$('.menuForm').on('click', function() {
			SHOW.apparition($('#experience'));
			SHOW.navigate($('#formation'), 'right');
		});
		$('.menuLoisir').on('click', function() {
			SHOW.apparition($('#experience'));
			SHOW.apparition($('#formation'));
			SHOW.navigate($('#loisir'), 'left');
		});
		$('.menuCont').on('click', function() {
			SHOW.apparition($('#experience'));
			SHOW.apparition($('#formation'));
			SHOW.apparition($('#loisir'));
			SHOW.navigate($('#contact'), 'right');
		});
	},

	apparition: function(section) { // APPARITION DE LA SECTION VOULUE
		section.css('display', 'flex');
	},

	navigate: function(anchor, direction) { // LANCEMENT DU SLIDE ET NAVIGATION VERS LA SECTION VOULUE
		if (window.matchMedia("(max-width: 768px)").matches) { // VERSION MOBILE
			$('#menu').hide("slow");
			$('#container').show();
			anchor.show('slide', {direction: direction}, 2000);
			$('html, body').animate({scrollTop: anchor.offset().top - $('#menu').height()}, 'slow');
		}

		else {
			anchor.show('slide', {direction: direction}, 2000);
			if (!$('nav').hasClass("navFixed")) {
				$('html, body').animate({scrollTop: anchor.offset().top - $("header").height()});
			}
			else {
				$('html, body').animate({scrollTop: anchor.offset().top - $("nav").height()});
			}
		}
	}
}

window.onload = INIT.init();
