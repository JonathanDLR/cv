ANIMATEBAR = {
    datavalue: [],
    value: [],
    spanbar: [],
    time: "",
    progressbar: [],
    interval: [],

    init: function() {
        ANIMATEBAR.progressbar = $("progress");
        ANIMATEBAR.spanbar = $('.progress-value');

        $.each(ANIMATEBAR.progressbar, function(i) {
            ANIMATEBAR.datavalue[i] = ANIMATEBAR.progressbar[i].getAttribute('data-value');
            ANIMATEBAR.value[i] = ANIMATEBAR.progressbar[i].value;
            ANIMATEBAR.time = (ANIMATEBAR.progressbar[i].getAttribute('max'))/4;

            ANIMATEBAR.loading();
        });    
    },

    loading: function() {
        var intv = setInterval(ANIMATEBAR.animate, ANIMATEBAR.time, ANIMATEBAR.interval.length); 
        ANIMATEBAR.interval.push(intv);      
    },

    animate: function(indexint) {
        ANIMATEBAR.value[indexint] +=1;
        ANIMATEBAR.progressbar[indexint].value = ANIMATEBAR.value[indexint];
        ANIMATEBAR.spanbar[indexint].innerHTML = ANIMATEBAR.value[indexint] + '%';
    
        if (ANIMATEBAR.value[indexint] >= ANIMATEBAR.datavalue[indexint]) {
            clearInterval(ANIMATEBAR.interval[indexint]);                                         
        }
    }
}

window.onload = ANIMATEBAR.init();