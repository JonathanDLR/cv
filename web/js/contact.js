CONTACT = {
    
    init: function() {
        $('#BUTenvoyer').on('click', CONTACT.sendcontact);
    },

    sendcontact: function() {
        var regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        
        if($('#INPnom').val() == "") {
            alert('Veuillez renseigner votre nom');
        }
        else if($('#INPmail').val() == "") {
            alert('Veuillez renseigner votre mail');
        }
        else if($('#INPobject').val() == "") {
            alert('Veuillez renseigner votre sujet');
        }
        else if($('#TEXcontent').val() == "") {
            alert('Veuillez renseigner votre message');
        }
        else if(!regex.test($("#INPmail").val())) {
            alert('Votre email n\est pas valide');
        }
        else {
            $.post(
                'index.php',
                {
                    nom: $('#INPnom').val(),
                    mail: $('#INPmail').val(),
                    object: $('#INPobject').val(),
                    content: $('#TEXcontent').val()
                },
                function success() {
                    alert("Votre message a bien été envoyé");
                }
            )
        }
       
    }
}

window.onload = CONTACT.init();